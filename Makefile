-include Makefile.cfg
.SUFFIXES:

ifndef $(VERSION)
VERSION=$(shell git describe --tags)
endif

VARIANTS=standard/supalape standard/supalape-bold standard/supalape-light basic/supalape basic/supalape-bold basic/supalape-light
OUTFONTS=$(patsubst %,%.otf,$(VARIANTS)) $(patsubst %,%.woff,$(VARIANTS))

all: $(OUTFONTS)

%.woff: %.sfd
	@mkdir -p $(@D)
	fontforge -lang=ff \
		-c 'Open($$1); Generate($$2)' \
		$< $*.woff
	@echo
%.otf: %.sfd
	@mkdir -p $(@D)
	fontforge -lang=ff \
		-c 'Open($$1); Generate($$2)' \
		$< $*.otf
	@echo
basic/supalape.sfd: supalape
	@mkdir -p $(@D)
	./supalape --no-ligatures > $@
basic/supalape-bold.sfd: supalape
	@mkdir -p $(@D)
	./supalape --light --no-ligatures > $@
basic/supalape-light.sfd: supalape
	@mkdir -p $(@D)
	./supalape --bold --no-ligatures > $@
standard/supalape.sfd: supalape
	@mkdir -p $(@D)
	./supalape > $@
standard/supalape-bold.sfd: supalape
	@mkdir -p $(@D)
	./supalape --bold > $@
standard/supalape-light.sfd: supalape
	@mkdir -p $(@D)
	./supalape --light > $@

supalape: supalape.c
	gcc $< -g -o $@ -DFONT_VERSION="\"$(VERSION)\""
clean:
	rm supalape $(OUTFONTS)
