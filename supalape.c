#ifndef FONT_VERSION
#define FONT_VERSION "unknown"
#endif

/* insa pi supa lape, a sitelen pona font based on "Bedstead".
 *
 * This font uses the same code as "bedstead", but replaces the non-ASCII
 * characters in the set with characters from the toki pona writing system
 * "sitelen pona". All the characters and modifications of the mode are done by
 * jan Lentan (/dev/urandom) <dev.urandom@posteo.org>.
 *
 * The characters that are defined in the Under-ConScript Unicode Registry's
 * proposed sitelen pona definition
 * <https://www.kreativekorp.com/ucsur/charts/sitelen.html> are located at
 * codepoints ascribed to them by the standard. Alternate glyphs for the
 * characters, as well as characters not defined in the standard, are located
 * elsewhere in the Private Use Area using the U+FED00 ~ U+FEFFF range.
 *
 * Some characters in sitelen pona can't fit into a 7x5 pixel box. For those, a 
 * "double horizontal resolution" modifier is added, which tells the font
 * generator code to make the lines thinner and read 12 horizontal "pixels"
 * worth of data instead of 6.
 *
 * Some characters fulfill the role of zero-width spacers or composing
 * characters. In more elaborate sitelen pona fonts, these spacers would have
 * ligatures associated with them, so that characters can be stacked or
 * inserted into each other. A special flag, ZWIDTH, is used for these, which
 * tells the font generator to set the character's width to 0.
 */

/*
 * Many of the character bitmaps below formed the typeface embodied in
 * the SAA5050 series of character-generator chips originally made and
 * sold by British company Mullard in the early 1980s.  Copyright in the
 * typeface will still be owned by Mullard's corporate successors, but
 * under section 55 of the Copyright Designs and Patents Act 1988 that
 * copyright is no longer infringed by the production or use of
 * articles specifically designed or adapted for producing material in
 * that typeface.
 *
 * The rest of the glyphs, and all of the code in this file, were
 * written by Ben Harris <bjh21@bjh21.me.uk>, Simon Tatham
 * <anakin@pobox.com>, and Marnanel Thurman <marnanel@thurman.org.uk>
 * between 2009 and 2020.
 *
 * To the extent possible under law, Ben Harris, Simon Tatham, and
 * Marnanel Thurman have dedicated all copyright and related and
 * neighboring rights to this software and the embodied typeface to
 * the public domain worldwide.  This software and typeface are
 * distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */
/*
 * This is a program to construct an outline font from a bitmap.  It's
 * based on the character-rounding algorithm of the Mullard SAA5050
 * series of Teletext character generators, and thus works best on
 * character shapes in the same style of those of the SAA5050.  This
 * file includes all of the glyphs from the SAA5050, SAA5051, SAA5052,
 * SAA5053, SAA5054, SAA5055, SAA5056, and SAA5057.  The output is a
 * Spline Font Database file suitable for feeding to Fontforge.
 *
 * The character-smoothing algorithm of the SAA5050 and friends is
 * a fairly simple means of expanding a 5x9 pixel character to 10x18
 * pixels for use on an interlaced display.  All it does is to detect
 * 2x2 clumps of pixels containing a diagonal line and add a couple of
 * subpixels to it, like this:
 *
 * . #  -> . . # # -> . . # # or # . -> # # . . -> # # . .
 * # .     . . # #    . # # #    . #    # # . .    # # # .
 *         # # . .    # # # .           . . # #    . # # #
 *         # # . .    # # . .           . . # #    . . # #
 *
 * This is applied to every occurrence of these patterns, even when
 * they overlap, and the result is that thin diagonal lines are
 * smoothed out while other features mostly remain the same.
 *
 * One way of extending this towards continuity would be to repeatedly
 * double the resolution and add more pixels to diagonals each time,
 * but this ends up with the diagonals being much too heavy.  Instead,
 * in places where the SAA5050 would add pixels, this program adds a
 * largeish triangle to each unfilled pixel, and removes a small
 * triangle from each filled one, something like this:
 *
 * . #  -> . . # # -> . . / # or # . -> # # . . -> # \ . .
 * # .     . . # #    . / # /    . #    # # . .    \ # \ .
 *         # # . .    / # / .           . . # #    . \ # \
 *         # # . .    # / . .           . . # #    . . \ #
 * 
 * The position of the lines is such that the diagonal stroke is the
 * same width as a horizontal or vertical stroke (assuming square
 * pixels).  There are a few additional complications, in that the
 * trimming of filled pixels can leave odd gaps where a diagonal stem
 * joins another one, so the code detects this and doesn't trim in
 * these cases:
 *
 * . # # -> . . # # # # -> . . / # # # -> . . / # # #
 * # . .    . . # # # #    . / # / # #    . / # # # #
 *          # # . . . .    / # / . . .    / # / . . .
 *          # # . . . .    # / . . . .    # / . . . .
 *
 * That is the interesting part of the program, and is in the dochar()
 * function.  Most of the rest is just dull geometry to join all the
 * bits together into a sensible outline.  Much of the code is wildly
 * inefficient -- O(n^2) algorithms aren't much of a problem when you
 * have at most a few thousand points to deal with.
 *
 * A rather nice feature of the outlines produced by this program is
 * that when rasterised at precisely 10 or 20 pixels high, they
 * produce the input and output respectively of the character-rounding
 * process.  While there are obious additional smoothings that could
 * be applied, doing so would probably lose this nice property.
 *
 * The glyph bitmaps included below include all the ones from the various
 * members of the SAA5050 family that I know about.  They are as shown
 * in the datasheet, and the English ones have been checked against a
 * real SAA5050.  Occasionally, different languages have different
 * glyphs for the same character -- these are represented as
 * alternates, with the default being the glyph that looks best.
 *
 * There are some extra glyphs included as well, some derived from
 * standard ones and some made from whole cloth.  They are built on
 * the same 5x9 matrix as the originals, and processed in the same way.
 */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define XSIZE 6
#define YSIZE 12

/*
 * Design parameters.  These can vary between fonts in the Bedstead family.
 *
 * To faithfully represent the output of an SAA5050, we need to know a
 * little about TV pixel aspect ratios:
 *
 * http://www.lurkertech.com/lg/video-systems/#sqnonsq
 *
 * The SAA5050 can be used to drive both 480i ("NTSC") and 576i
 * ("PAL") displays.  In 480i, industry convention is that you get
 * square pixels with a pixel clock of 12+3/11 MHz.  In 576i, you need
 * 14.75 MHz.  The SAA5050 takes a nominal 6MHz clock and uses both
 * edges to output subpixels (duty cycle between 0.4 and 0.6).  This
 * means that the nominal pixel aspect ratios are 12+3/11:12 for 480i
 * and 14.75:12 for 576i.  These correspond to pixel widths of 102.3
 * and 122.9 respectively.
 *
 * 102.3 is close enough to 100 that we may as well just use square
 * pixels, which will work better on modern displays.  Similarly,
 * 122.9 is close to 125 and keeping the widths as simple fractions
 * makes the fonts easier to work with.
 *
 * Double-height mode requires fonts half the usual width, hence the
 * existence of ultra condensed and extra condensed forms.  The other
 * two widths just fill in the gap with the obvious fractions of the
 * normal width.
 */

static struct width {
	char const * option;
	char const * suffix;
	double xpix;
	int ttfwidth;
} const widths[] = {
	{
		"--normal",
		"",
		100,		/* xpix */
		5,		/* ttfwidth */
	},
	{
		"--ultra-condensed",
		" Ultra Condensed",
		50,		/* xpix */
		1,		/* ttfwidth */
	},
	{
		"--extra-condensed",
		" Extra Condensed",
		62.5,		/* xpix */
		2,		/* ttfwidth */
	},
	{
		"--condensed",
		" Condensed",
		75,		/* xpix */
		3,		/* ttfwidth */
	},
	{
		"--semi-condensed",
		" Semi Condensed",
		87.5,		/* xpix */
		4,		/* ttfwidth */
	},
	{
		"--extended",
		" Extended",
		125,		/* xpix */
		7,		/* ttfwidth */
	},
};
static int const nwidths = sizeof(widths) / sizeof(widths[0]);

struct width const *width = &widths[0];

/* Size of output pixels in font design units (usually 1000/em) */
#define XPIX (width->xpix)
#define YPIX 100

/* Internally, we work in pixels 100 design units square */
#define XPIX_S 100
#define YPIX_S 100

#define XSCALE ((double)XPIX_S / (double)XPIX)
#define YSCALE ((double)YPIX_S / (double)YPIX)

/* Position of diagonal lines within pixels */
/* 29 is approximately 100 * (1-1/sqrt(2)) */
#define XQTR_S 29
#define YQTR_S 29

static struct weight {
	char const *option;
	char const *suffix;
	int weight; /* Expressed in internal units */
	int ttfweight;
} const weights[] = {
	{
		"--medium",
		"",
		0,
		500,
	},
	{
		"--light",
		" Light",
		2 * XQTR_S - 100,
		300,
	},
	{
		"--bold",
		" Bold",
		+50,
		700,
	},
};

static int const nweights = sizeof(weights) / sizeof(weights[0]);

struct weight const *weight = &weights[0];

static void dochar(short const data[YSIZE], unsigned flags);
static void dochar_plotter(short const data[YSIZE], unsigned flags);
static void domosaic(unsigned code, bool sep);
static void domosaic4(unsigned code, bool sep);
static void doalias(char const *alias_of);
static void dopanose(void);
static void glyph_complement(void);

/* U(N) sets the code point and name of a glyph not in AGLFN */
#define U(N) 0x ## N, 0x ## N >= 0x10000 ? "u" #N : "uni" #N
#define ALIAS(alias, canonical) {{},-1,alias,0,canonical}

static struct glyph {
	short data[YSIZE];
	int unicode;
	char const *name;
	unsigned int flags;
#define SEP  0x01 /* Separated graphics */
#define MOS6 0x02 /* 6-cell mosaic graphics character */
#define MOS4 0x04 /* 4-cell mosaic graphics character */
#define DBLXRES 0x08 /* Double the X resolution */
#define ZWIDTH 0x10 /* Zero width (for composite chars) */
#define NIMI_PONA 0x20 /* valid character for being composed into a cartouche */
#define NIMI_OPEN 0x40 /* starting character for cartouches */
#define NIMI_PINI 0x80 /* ending character for cartouches */
#define COMPOSITE 0x100 /* composite character, shift all X by -600 */
#define NIMI_INSA 0x200 /* middle character for cartouches */
#define COMPOSED 0x400 /* this is a precomposed character */
#define SEP6 (SEP | MOS6)
#define SEP4 (SEP | MOS4)
	char const *alias_of;
	char const **alt_ligatures;
	char const* substitute;
} const _glyphs[] = {
	/*
	 * The first batch of glyphs comes from the code tables at the end of
	 * the Mullard SAA5050 series datasheet, dated July 1982.
	 */
	/* US ASCII (SAA5055) character set */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0x0020, "space", NIMI_PONA },
	{{000,000,004,004,004,004,004,000,004,000,000}, 0x0021, "exclam", NIMI_PONA },
	{{000,000,012,012,012,000,000,000,000,000,000}, 0x0022, "quotedbl", NIMI_PONA },
	{{000,000,012,012,037,012,037,012,012,000,000}, 0x0023, "numbersign", NIMI_PONA },
	{{000,000,016,025,024,016,005,025,016,000,000}, 0x0024, "dollar", NIMI_PONA },
	{{000,000,030,031,002,004,010,023,003,000,000}, 0x0025, "percent", NIMI_PONA },
	{{000,000,010,024,024,010,025,022,015,000,000}, 0x0026, "ampersand", NIMI_PONA },
	{{000,000,004,004,010,000,000,000,000,000,000}, 0x0027, "quotesingle", NIMI_PONA },
	{{000,000,004,004,010,000,000,000,000,000,000}, 0x2019, "quoteright", NIMI_PONA },
	{{000,000,002,004,010,010,010,004,002,000,000}, 0x0028, "parenleft", NIMI_PONA },
	{{000,000,010,004,002,002,002,004,010,000,000}, 0x0029, "parenright", NIMI_PONA },
	{{000,000,004,025,016,004,016,025,004,000,000}, 0x002a, "asterisk", NIMI_PONA },
	{{000,000,000,004,004,037,004,004,000,000,000}, 0x002b, "plus", NIMI_PONA },
	{{000,000,000,000,000,000,000,004,004,010,000}, 0x002c, "comma", NIMI_PONA },
	{{000,000,000,000,000,016,000,000,000,000,000}, 0x002d, "hyphen", NIMI_PONA },
	{{000,000,000,000,000,000,000,000,004,000,000}, 0x002e, "period", NIMI_PONA },
	{{000,000,000,001,002,004,010,020,000,000,000}, 0x002f, "slash", NIMI_PONA },
	{{000,000,004,012,021,021,021,012,004,000,000}, 0x0030, "zero", NIMI_PONA },
	{{000,000,004,014,004,004,004,004,016,000,000}, 0x0031, "one", NIMI_PONA },
	{{000,000,016,021,001,006,010,020,037,000,000}, 0x0032, "two", NIMI_PONA },
	{{000,000,037,001,002,006,001,021,016,000,000}, 0x0033, "three", NIMI_PONA },
	{{000,000,002,006,012,022,037,002,002,000,000}, 0x0034, "four", NIMI_PONA },
	{{000,000,037,020,036,001,001,021,016,000,000}, 0x0035, "five", NIMI_PONA },
	{{000,000,006,010,020,036,021,021,016,000,000}, 0x0036, "six", NIMI_PONA },
	{{000,000,037,001,002,004,010,010,010,000,000}, 0x0037, "seven", NIMI_PONA },
	{{000,000,016,021,021,016,021,021,016,000,000}, 0x0038, "eight", NIMI_PONA },
	{{000,000,016,021,021,017,001,002,014,000,000}, 0x0039, "nine", NIMI_PONA },
	{{000,000,000,000,004,000,000,000,004,000,000}, 0x003a, "colon", NIMI_PONA },
	{{000,000,000,000,004,000,000,004,004,010,000}, 0x003b, "semicolon", NIMI_PONA},
	{{000,000,002,004,010,020,010,004,002,000,000}, 0x003c, "less", NIMI_PONA },
	{{000,000,000,000,037,000,037,000,000,000,000}, 0x003d, "equal", NIMI_PONA },
	{{000,000,010,004,002,001,002,004,010,000,000}, 0x003e, "greater", NIMI_PONA },
	{{000,000,016,021,002,004,004,000,004,000,000}, 0x003f, "question", NIMI_PONA },
	{{000,000,016,021,027,025,027,020,016,000,000}, 0x0040, "at", NIMI_PONA },
	{{000,000,004,012,021,021,037,021,021,000,000}, 0x0041, "A", NIMI_PONA },
	{{000,000,036,021,021,036,021,021,036,000,000}, 0x0042, "B", NIMI_PONA },
	{{000,000,016,021,020,020,020,021,016,000,000}, 0x0043, "C", NIMI_PONA },
	{{000,000,036,021,021,021,021,021,036,000,000}, 0x0044, "D", NIMI_PONA },
	{{000,000,037,020,020,036,020,020,037,000,000}, 0x0045, "E", NIMI_PONA },
	{{000,000,037,020,020,036,020,020,020,000,000}, 0x0046, "F", NIMI_PONA },
	{{000,000,016,021,020,020,023,021,017,000,000}, 0x0047, "G", NIMI_PONA },
	{{000,000,021,021,021,037,021,021,021,000,000}, 0x0048, "H", NIMI_PONA },
	{{000,000,016,004,004,004,004,004,016,000,000}, 0x0049, "I", NIMI_PONA },
	{{000,000,001,001,001,001,001,021,016,000,000}, 0x004a, "J", NIMI_PONA },
	{{000,000,021,022,024,030,024,022,021,000,000}, 0x004b, "K", NIMI_PONA },
	{{000,000,020,020,020,020,020,020,037,000,000}, 0x004c, "L", NIMI_PONA },
	{{000,000,021,033,025,025,021,021,021,000,000}, 0x004d, "M", NIMI_PONA },
	{{000,000,021,021,031,025,023,021,021,000,000}, 0x004e, "N", NIMI_PONA },
	{{000,000,016,021,021,021,021,021,016,000,000}, 0x004f, "O", NIMI_PONA },
	{{000,000,036,021,021,036,020,020,020,000,000}, 0x0050, "P", NIMI_PONA },
	{{000,000,016,021,021,021,025,022,015,000,000}, 0x0051, "Q", NIMI_PONA },
	{{000,000,036,021,021,036,024,022,021,000,000}, 0x0052, "R", NIMI_PONA },
	{{000,000,016,021,020,016,001,021,016,000,000}, 0x0053, "S", NIMI_PONA },
	{{000,000,037,004,004,004,004,004,004,000,000}, 0x0054, "T", NIMI_PONA },
	{{000,000,021,021,021,021,021,021,016,000,000}, 0x0055, "U", NIMI_PONA },
	{{000,000,021,021,021,012,012,004,004,000,000}, 0x0056, "V", NIMI_PONA },
	{{000,000,021,021,021,025,025,025,012,000,000}, 0x0057, "W", NIMI_PONA },
	{{000,000,021,021,012,004,012,021,021,000,000}, 0x0058, "X", NIMI_PONA },
	{{000,000,021,021,012,004,004,004,004,000,000}, 0x0059, "Y", NIMI_PONA },
	{{000,000,037,001,002,004,010,020,037,000,000}, 0x005a, "Z", NIMI_PONA },
	{{000,000,017,010,010,010,010,010,017,000,000}, 0x005b, "bracketleft", NIMI_PONA },
	{{000,000,000,020,010,004,002,001,000,000,000}, 0x005c, "backslash", NIMI_PONA },
	{{000,000,036,002,002,002,002,002,036,000,000}, 0x005d, "bracketright", NIMI_PONA },
	{{000,000,004,012,021,000,000,000,000,000,000}, 0x005e, "asciicircum", NIMI_PONA },
	{{000,000,000,000,000,000,000,000,037,000,000}, 0x005f, "underscore", NIMI_PONA },
	{{000,000,004,004,002,000,000,000,000,000,000}, 0x201b, "quotereversed", NIMI_PONA },
	{{000,000,000,000,016,001,017,021,017,000,000}, 0x0061, "a", NIMI_PONA },
	{{000,000,020,020,036,021,021,021,036,000,000}, 0x0062, "b", NIMI_PONA },
	{{000,000,000,000,017,020,020,020,017,000,000}, 0x0063, "c", NIMI_PONA },
	{{000,000,001,001,017,021,021,021,017,000,000}, 0x0064, "d", NIMI_PONA },
	{{000,000,000,000,016,021,037,020,016,000,000}, 0x0065, "e", NIMI_PONA },
	{{000,000,002,004,004,016,004,004,004,000,000}, 0x0066, "f", NIMI_PONA },
	{{000,000,000,000,017,021,021,021,017,001,016}, 0x0067, "g", NIMI_PONA },
	{{000,000,020,020,036,021,021,021,021,000,000}, 0x0068, "h", NIMI_PONA },
	{{000,000,004,000,014,004,004,004,016,000,000}, 0x0069, "i", NIMI_PONA },
	{{000,000,004,000,004,004,004,004,004,004,010}, 0x006a, "j", NIMI_PONA },
	{{000,000,010,010,011,012,014,012,011,000,000}, 0x006b, "k", NIMI_PONA },
	{{000,000,014,004,004,004,004,004,016,000,000}, 0x006c, "l", NIMI_PONA },
	{{000,000,000,000,032,025,025,025,025,000,000}, 0x006d, "m", NIMI_PONA },
	{{000,000,000,000,036,021,021,021,021,000,000}, 0x006e, "n", NIMI_PONA },
	{{000,000,000,000,016,021,021,021,016,000,000}, 0x006f, "o", NIMI_PONA },
	{{000,000,000,000,036,021,021,021,036,020,020}, 0x0070, "p", NIMI_PONA },
	{{000,000,000,000,017,021,021,021,017,001,001}, 0x0071, "q", NIMI_PONA },
	{{000,000,000,000,013,014,010,010,010,000,000}, 0x0072, "r", NIMI_PONA },
	{{000,000,000,000,017,020,016,001,036,000,000}, 0x0073, "s", NIMI_PONA },
	{{000,000,004,004,016,004,004,004,002,000,000}, 0x0074, "t", NIMI_PONA },
	{{000,000,000,000,021,021,021,021,017,000,000}, 0x0075, "u", NIMI_PONA },
	{{000,000,000,000,021,021,012,012,004,000,000}, 0x0076, "v", NIMI_PONA },
	{{000,000,000,000,021,021,025,025,012,000,000}, 0x0077, "w", NIMI_PONA },
	{{000,000,000,000,021,012,004,012,021,000,000}, 0x0078, "x", NIMI_PONA },
	{{000,000,000,000,021,021,021,021,017,001,016}, 0x0079, "y", NIMI_PONA },
	{{000,000,000,000,037,002,004,010,037,000,000}, 0x007a, "z", NIMI_PONA },
	{{000,000,003,004,004,010,004,004,003,000,000}, 0x007b, "braceleft", NIMI_PONA },
	{{000,000,004,004,004,000,004,004,004,000,000}, 0x00a6, "brokenbar", NIMI_PONA },
	{{000,000,030,004,004,002,004,004,030,000,000}, 0x007d, "braceright", NIMI_PONA },
	{{000,000,010,025,002,000,000,000,000,000,000}, 0x007e, "asciitilde", NIMI_PONA },
	{{000,000,025,021,000,021,000,021,025,000,000}, 0x007f, "uni007F", NIMI_PONA },
	{{000,000,037,037,037,037,037,037,037,000,000}, 0x25a0, "filledbox", NIMI_PONA },

	/*
	 * The other batch of glyphs were specially designed for this font.
	 * These are kept sorted by Unicode code point.
	 */

	/* Basic Latin */
	{{000,000,010,004,002,000,000,000,000,000,000}, 0x0060, "grave", NIMI_PONA },
	{{000,000,004,004,004,004,004,004,004,000,000}, 0x007c, "bar", NIMI_PONA },
	
	/* ZWJ */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0x200D, "zwj",ZWIDTH }, /* ZWJ */

	/* Private use */

	/* sitelen pona */
	{{000,000,004,004,000,017,021,023,015,000,000}, 0xF1900, "sp_a",NIMI_PONA,0,NULL,"spa_a"}, /* a */
	{{000,000,012,000,016,021,037,021,016,000,000}, 0xF1901, "sp_akesi", NIMI_PONA }, /* akesi */
	{{000,000,000,021,012,004,012,021,000,000,000}, 0xF1902, "sp_ala", NIMI_PONA }, /* ala */
	{{000,000,014,012,012,027,012,012,014,000,000}, 0xF1903, "sp_alasa", NIMI_PONA }, /* alasa */
	{{000,000,000,013,025,025,025,032,000,000,000}, 0xF1904, "sp_ale", NIMI_PONA, 0,(const char*[]){"a l i","a l i space",NULL} }, /* ale */
	{{000,000,021,021,021,037,000,004,004,000,000}, 0xF1905, "sp_anpa", NIMI_PONA }, /* anpa */
	{{000,000,021,012,004,000,004,012,021,000,000}, 0xF1906, "sp_ante", NIMI_PONA }, /* ante */
	{{000,000,021,021,012,012,004,004,004,000,000}, 0xF1907, "sp_anu", NIMI_PONA }, /* anu */
	{{000,000,004,004,012,012,012,012,033,000,000}, 0xF1908, "sp_awen", NIMI_PONA }, /* awen */
	{{000,000,000,000,022,011,022,000,000,000,000}, 0xF1909, "sp_e", NIMI_PONA }, /* e */
	{{000,000,000,004,004,037,004,004,000,000,000}, 0xF190A, "sp_en", NIMI_PONA }, /* en */
	{{000,000,006,025,016,004,016,025,014,000,000}, 0xF190B, "sp_esun", NIMI_PONA }, /* esun */
	{{000,000,006,011,021,021,021,022,014,000,000}, 0xF190C, "sp_ijo", NIMI_PONA }, /* ijo */
	{{000,000,000,000,016,021,021,000,000,000,000}, 0xF190D, "sp_ike", NIMI_PONA }, /* ike */
	{{000,000,037,025,025,037,004,004,004,000,000}, 0xF190E, "sp_ilo", NIMI_PONA }, /* ilo */
	{{000,000,000,021,025,025,021,037,000,000,000}, 0xF190F, "sp_insa", NIMI_PONA }, /* insa */
	{{000,000,004,005,005,016,025,025,011,000,000}, 0xF1910, "sp_jaki", NIMI_PONA ,0,NULL,"spa_jaki" }, /* jaki */
	{{000,000,016,021,021,021,016,012,021,000,000}, 0xF1911, "sp_jan", NIMI_PONA }, /* jan */
	{{000,000,004,016,033,016,012,021,037,000,000}, 0xF1912, "sp_jelo", NIMI_PONA }, /* jelo */
	{{000,000,016,012,016,020,027,021,016,000,000}, 0xF1913, "sp_jo", NIMI_PONA }, /* jo */
	{{000,000,000,000,026,011,026,000,000,000,000}, 0xF1914, "sp_kala", NIMI_PONA }, /* kala */
	{{000,000,025,025,000,037,021,021,016,000,000}, 0xF1915, "sp_kalama", NIMI_PONA }, /* kalama */
	{{000,000,010,014,012,011,011,011,033,000,000}, 0xF1916, "sp_kama", NIMI_PONA }, /* kama */
	{{000,000,000,033,025,025,016,004,004,000,000}, 0xF1917, "sp_kasi", NIMI_PONA }, /* kasi */
	{{000,000,021,021,022,034,022,021,021,000,000}, 0xF1918, "sp_ken", NIMI_PONA }, /* ken */
	{{000,000,037,025,037,004,026,031,021,000,000}, 0xF1919, "sp_kepeken", NIMI_PONA }, /* kepeken */
	{{000,000,002,004,012,025,021,021,016,000,000}, 0xF191A, "sp_kili", NIMI_PONA }, /* kili */
	{{000,000,000,016,021,021,012,004,000,000,000}, 0xF191B, "sp_kiwen", NIMI_PONA }, /* kiwen */
	{{000,000,016,022,021,011,022,021,016,000,000}, 0xF191C, "sp_ko", NIMI_PONA }, /* ko */
	{{000,000,011,022,022,033,011,011,022,000,000}, 0xF191D, "sp_kon", NIMI_PONA }, /* kon */
	{{000,000,004,004,012,037,012,021,037,000,000}, 0xF191E, "sp_kule", NIMI_PONA }, /* kule */
	{{000,000,016,016,000,000,033,033,033,000,000}, 0xF191F, "sp_kulupu", NIMI_PONA }, /* kulupu */
	{{000,000,016,021,005,011,002,004,010,000,000}, 0xF1920, "sp_kute", NIMI_PONA }, /* kute */
	{{000,000,010,004,002,002,002,004,010,000,000}, 0xF1921, "sp_la", NIMI_PONA }, /* la */
	{{000,000,000,000,007,035,007,000,000,000,000}, 0xF1922, "sp_lape", NIMI_PONA }, /* lape */
	{{000,000,033,025,016,004,012,021,037,000,000}, 0xF1923, "sp_laso", NIMI_PONA }, /* laso */
	{{000,000,014,022,037,022,022,022,014,000,000}, 0xF1924, "sp_lawa", NIMI_PONA }, /* lawa */
	{{000,000,036,021,021,021,037,025,025,000,000}, 0xF1925, "sp_len", NIMI_PONA }, /* len */
	{{000,000,000,012,004,037,004,012,000,000,000}, 0xF1926, "sp_lete", NIMI_PONA }, /* lete */
	{{000,000,010,004,002,001,002,004,010,000,000}, 0xF1927, "sp_li", NIMI_PONA }, /* li */
	{{000,000,000,000,000,012,004,000,000,000,000}, 0xF1928, "sp_lili", NIMI_PONA }, /* lili */
	{{000,000,000,000,002,025,010,000,000,000,000}, 0xF1929, "sp_linja", NIMI_PONA }, /* linja */
	{{000,000,017,021,021,021,021,021,037,000,000}, 0xF192A, "sp_lipu", NIMI_PONA }, /* lipu */
	{{000,000,037,021,016,004,012,021,037,000,000}, 0xF192B, "sp_loje", NIMI_PONA }, /* loje */
	{{000,000,000,000,004,000,037,000,000,000,000}, 0xF192C, "sp_lon", NIMI_PONA }, /* lon */
	{{000,000,006,011,011,011,031,021,011,000,000}, 0xF192D, "sp_luka", NIMI_PONA }, /* luka */
	{{000,000,016,021,021,025,021,021,016,000,000}, 0xF192E, "sp_lukin", NIMI_PONA }, /* lukin */
	{{000,000,021,021,021,021,021,021,016,000,000}, 0xF192F, "sp_lupa", NIMI_PONA }, /* lupa */
	{{000,000,000,016,025,037,025,016,000,000,000}, 0xF1930, "sp_ma", NIMI_PONA }, /* ma */
	{{000,000,016,021,021,021,016,012,004,000,000}, 0xF1931, "sp_mama", NIMI_PONA }, /* mama */
	{{000,000,021,016,021,021,021,016,000,000,000}, 0xF1932, "sp_mani", NIMI_PONA }, /* mani */
	{{000,000,016,021,025,033,025,021,000,000,000}, 0xF1933, "sp_meli", NIMI_PONA }, /* meli */
	{{000,000,016,021,021,022,034,020,020,000,000}, 0xF1934, "sp_mi", NIMI_PONA }, /* mi */
	{{000,000,016,021,021,021,012,037,021,000,000}, 0xF1935, "sp_mije", NIMI_PONA }, /* mije */
	{{000,000,037,021,016,000,026,031,021,000,000}, 0xF1936, "sp_moku", NIMI_PONA }, /* moku */
	{{000,000,016,037,025,037,016,000,016,000,000}, 0xF1937, "sp_moli", NIMI_PONA }, /* moli */
	{{000,000,000,007,004,024,004,007,000,000,000}, 0xF1938, "sp_monsi", NIMI_PONA }, /* monsi */
	{{000,000,033,033,016,021,025,021,016,000,000}, 0xF1939, "sp_mu", NIMI_PONA }, /* mu */
	{{000,000,034,022,011,011,011,022,034,000,000}, 0xF193A, "sp_mun", NIMI_PONA }, /* mun */
	{{000,000,033,033,033,021,021,021,016,000,000}, 0xF193B, "sp_musi", NIMI_PONA }, /* musi */
	{{000,000,025,025,025,025,025,025,025,000,000}, 0xF193C, "sp_mute", NIMI_PONA }, /* mute */
	{{000,000,012,037,012,012,012,037,012,000,000}, 0xF193D, "sp_nanpa", NIMI_PONA }, /* nanpa */
	{{000,000,006,010,022,025,025,021,016,000,000}, 0xF193E, "sp_nasa", NIMI_PONA }, /* nasa */
	{{000,000,004,004,016,025,004,004,004,000,000}, 0xF193F, "sp_nasin", NIMI_PONA }, /* nasin */
	{{000,000,016,021,021,021,021,021,021,000,000}, 0xF1940, "sp_nena", NIMI_PONA }, /* nena */
	{{000,000,004,004,004,004,025,016,004,000,000}, 0xF1941, "sp_ni", NIMI_PONA }, /* ni */
	{{000,000,000,000,017,021,036,000,000,000,000}, 0xF1942, "sp_nimi", NIMI_PONA ,0,NULL,"spa_nimi" }, /* nimi */
	{{000,000,024,024,024,024,026,021,036,000,000}, 0xF1943, "sp_noka", NIMI_PONA }, /* noka */
	{{000,000,004,004,000,016,021,021,016,000,000}, 0xF1944, "sp_o", NIMI_PONA, 0,NULL,"spa_o" }, /* o */
	{{000,000,016,000,012,025,021,012,004,000,000}, 0xF1945, "sp_olin", NIMI_PONA }, /* olin */
	{{000,000,000,006,011,011,011,036,000,000,000}, 0xF1946, "sp_ona", NIMI_PONA }, /* ona */
	{{000,000,021,021,021,021,037,021,037,000,000}, 0xF1947, "sp_open", NIMI_PONA }, /* open */
	{{000,000,037,004,010,016,002,004,037,000,000}, 0xF1948, "sp_pakala", NIMI_PONA }, /* pakala */
	{{000,000,006,011,011,006,011,031,011,000,000}, 0xF1949, "sp_pali", NIMI_PONA }, /* pali */
	{{000,000,004,012,012,012,012,012,004,000,000}, 0xF194A, "sp_palisa", NIMI_PONA }, /* palisa */
	{{000,000,033,004,033,004,033,004,004,000,000}, 0xF194B, "sp_pan", NIMI_PONA, 0,NULL,"spa_pan" }, /* pan */
	{{000,000,025,025,000,006,011,031,011,000,000}, 0xF194C, "sp_pana", NIMI_PONA }, /* pana */
	{{000,000,000,020,020,020,020,037,000,000,000}, 0xF194D, "sp_pi", NIMI_PONA }, /* pi */
	{{000,000,000,012,025,021,012,004,000,000,000}, 0xF194E, "sp_pilin", NIMI_PONA }, /* pilin */
	{{000,000,004,004,016,016,037,037,037,000,000}, 0xF194F, "sp_pimeja", NIMI_PONA }, /* pimeja */
	{{000,000,000,016,004,004,004,016,000,000,000}, 0xF1950, "sp_pini", NIMI_PONA }, /* pini */
	{{000,000,012,000,004,016,004,016,004,000,000}, 0xF1951, "sp_pipi", NIMI_PONA }, /* pipi */
	{{000,000,000,024,024,025,024,034,000,000,000}, 0xF1952, "sp_poka", NIMI_PONA }, /* poka */
	{{000,000,000,021,021,021,021,037,000,000,000}, 0xF1953, "sp_poki", NIMI_PONA }, /* poki */
	{{000,000,000,000,021,021,016,000,000,000,000}, 0xF1954, "sp_pona", NIMI_PONA }, /* pona */
	{{000,000,037,025,037,033,021,033,037,000,000}, 0xF1955, "sp_pu", NIMI_PONA }, /* pu */
	{{000,000,000,037,000,000,000,037,000,000,000}, 0xF1956, "sp_sama", NIMI_PONA }, /* sama */
	{{000,000,004,025,025,025,000,016,016,000,000}, 0xF1957, "sp_seli", NIMI_PONA }, /* seli */
	{{000,000,000,01777,01111,01111,01111,01111,000,000,000}, 0xF1958, "sp_selo", DBLXRES | NIMI_PONA }, /* selo */
	{{000,000,016,021,035,011,016,012,016,000,000}, 0xF1959, "sp_seme", NIMI_PONA, 0,NULL,"spa_seme" }, /* seme */
	{{000,000,001,001,005,025,025,025,012,000,000}, 0xF195A, "sp_sewi", NIMI_PONA }, /* sewi */
	{{000,000,037,025,025,025,025,025,025,000,000}, 0xF195B, "sp_sijelo", NIMI_PONA }, /* sijelo */
	{{000,000,000,016,025,033,025,016,000,000,000}, 0xF195C, "sp_sike", NIMI_PONA }, /* sike */
	{{000,000,004,004,000,033,000,000,000,000,000}, 0xF195D, "sp_sin", NIMI_PONA }, /* sin */
	{{000,000,020,020,026,031,021,021,016,000,000}, 0xF195E, "sp_sina", NIMI_PONA }, /* sina */
	{{000,000,000,034,004,005,004,034,000,000,000}, 0xF195F, "sp_sinpin", NIMI_PONA }, /* sinpin */
	{{000,000,037,021,025,021,025,021,037,000,000}, 0xF1960, "sp_sitelen", NIMI_PONA }, /* sitelen */
	{{000,000,025,025,000,037,021,021,037,000,000}, 0xF1961, "sp_sona", NIMI_PONA }, /* sona */
	{{000,000,036,001,025,001,006,004,004,000,000}, 0xF1962, "sp_soweli", NIMI_PONA }, /* soweli */
	{{000,000,021,021,021,012,012,012,004,000,000}, 0xF1963, "sp_suli", NIMI_PONA }, /* suli */
	{{000,000,004,025,012,021,012,025,004,000,000}, 0xF1964, "sp_suno", NIMI_PONA }, /* suno */
	{{000,000,000,037,012,012,012,012,000,000,000}, 0xF1965, "sp_supa", NIMI_PONA }, /* supa */
	{{000,000,000,012,025,000,000,004,000,000,000}, 0xF1966, "sp_suwi", NIMI_PONA }, /* suwi */
	{{000,000,004,010,036,011,005,001,001,000,000}, 0xF1967, "sp_tan", NIMI_PONA }, /* tan */
	{{000,000,002,002,002,016,002,002,002,000,000}, 0xF1968, "sp_taso", NIMI_PONA }, /* taso */
	{{000,000,002,006,012,022,022,022,033,000,000}, 0xF1969, "sp_tawa", NIMI_PONA }, /* tawa */
	{{000,000,010,025,002,000,010,025,002,000,000}, 0xF196A, "sp_telo", NIMI_PONA }, /* telo */
	{{000,000,016,021,025,027,021,021,016,000,000}, 0xF196B, "sp_tenpo", NIMI_PONA }, /* tenpo */
	{{000,000,025,025,000,016,021,021,016,000,000}, 0xF196C, "sp_toki", NIMI_PONA }, /* toki */
	{{000,000,004,012,021,021,021,021,037,000,000}, 0xF196D, "sp_tomo", NIMI_PONA }, /* tomo */
	{{000,000,012,012,012,012,012,012,012,000,000}, 0xF196E, "sp_tu", NIMI_PONA }, /* tu */
	{{000,000,012,025,025,025,012,012,004,000,000}, 0xF196F, "sp_unpa", NIMI_PONA }, /* unpa */
	{{000,000,000,037,021,021,016,000,004,000,000}, 0xF1970, "sp_uta", NIMI_PONA }, /* uta */
	{{000,000,000,021,012,004,033,033,000,000,000}, 0xF1971, "sp_utala", NIMI_PONA }, /* utala */
	{{000,000,004,025,012,012,021,021,037,000,000}, 0xF1972, "sp_walo", NIMI_PONA }, /* walo */
	{{000,000,004,014,024,004,004,004,004,000,000}, 0xF1973, "sp_wan", NIMI_PONA }, /* wan */
	{{000,000,004,002,025,001,017,010,010,000,000}, 0xF1974, "sp_waso", NIMI_PONA }, /* waso */
	{{000,000,021,021,021,004,012,012,004,000,000}, 0xF1975, "sp_wawa", NIMI_PONA }, /* wawa */
	{{000,000,000,021,012,000,012,021,000,000,000}, 0xF1976, "sp_weka", NIMI_PONA }, /* weka */
	{{000,000,012,021,021,025,025,025,012,000,000}, 0xF1977, "sp_wile", NIMI_PONA, 0,NULL,"spa_wile" }, /* wile */
	{{000,000,004,004,000,033,000,004,004,000,000}, 0xF1978, "sp_namako", NIMI_PONA }, /* namako */
	{{000,000,004,004,004,000,025,016,025,000,000}, 0xF1979, "sp_kin", NIMI_PONA, 0,NULL,"spa_kin" }, /* kin */
	{{000,000,002,004,010,024,010,004,002,000,000}, 0xF197A, "sp_oko", NIMI_PONA }, /* oko */
	{{000,000,000,021,002,004,010,021,000,000,000}, 0xF197B, "sp_kipisi", NIMI_PONA }, /* kipisi */
	{{000,000,000,037,021,025,021,037,000,000,000}, 0xF197C, "sp_leko", NIMI_PONA }, /* leko */
	{{000,000,012,012,012,025,025,025,021,000,000}, 0xF197D, "sp_monsuta", NIMI_PONA }, /* monsuta */
	{{000,000,021,016,021,021,021,016,004,000,000}, 0xF197E, "sp_tonsi", NIMI_PONA }, /* tonsi */
	{{000,000,004,026,015,004,026,015,004,000,000}, 0xF197F, "sp_jasima", NIMI_PONA }, /* jasima */
	{{000,000,00174,00202,00251,00202,00624,01224,00724,000,000}, 0xF1980, "sp_kijetesantakalu", DBLXRES | NIMI_PONA }, /* kijetesantakalu */
	{{000,000,016,021,021,037,012,012,016,000,000}, 0xF1981, "sp_soko", NIMI_PONA }, /* soko */
	{{000,000,021,021,021,025,021,021,021,000,000}, 0xF1982, "sp_meso", NIMI_PONA }, /* meso */
	{{000,000,004,016,025,004,004,004,004,000,000}, 0xF1983, "sp_epiku", NIMI_PONA }, /* epiku */
	{{000,000,025,000,015,022,025,011,026,000,000}, 0xF1984, "sp_kokosila", NIMI_PONA }, /* kokosila */
	{{000,000,002,015,022,020,023,024,011,000,000}, 0xF1985, "sp_lanpan", NIMI_PONA }, /* lanpan */
	{{000,000,002,004,000,026,031,021,021,000,000}, 0xF1986, "sp_n", NIMI_PONA }, /* n */
	{{000,000,016,021,021,037,021,021,016,000,000}, 0xF1987, "sp_misikeke", NIMI_PONA }, /* misikeke */
	{{000,000,07776,06606,06776,06606,06326,06306,07776,000,000}, 0xF1988, "sp_ku", DBLXRES | NIMI_PONA }, /* ku */
	{{000,000,037,004,004,004,004,004,004,000,000}, 0xF19A0, "sp_pake", NIMI_PONA }, /* pake */
	{{000,000,033,033,000,004,012,021,000,000,000}, 0xF19A1, "sp_apeja", NIMI_PONA }, /* apeja */
	{{000,000,000,000,033,000,004,004,000,000,000}, 0xF19A2, "sp_majuna", NIMI_PONA }, /* majuna */
	{{000,000,000,012,004,012,000,000,037,000,000}, 0xF19A3, "sp_powe", NIMI_PONA }, /* powe */
	
	{{000,000,016,010,004,004,012,012,004,000,000}, 0xF2000, "spa_a", NIMI_PONA }, /* alternate a */
	{{000,000,016,002,004,004,012,012,004,000,000}, 0xF2001, "spa_kin", NIMI_PONA }, /* alternate kin */
	{{000,000,004,004,004,004,012,012,004,000,000}, 0xF2002, "spa_o", NIMI_PONA }, /* alternate o */
	{{000,000,023,025,025,016,025,024,030,000,000}, 0xF2003, "spa_jaki", NIMI_PONA }, /* alternate jaki */
	{{000,000,000,037,023,025,031,037,000,000,000}, 0xF2004, "spa_nimi", NIMI_PONA }, /* alternate nimi */
	{{000,000,021,012,004,000,021,012,004,000,000}, 0xF2005, "spa_pan", NIMI_PONA }, /* alternate pan */
	{{000,000,004,012,021,021,025,025,012,000,000}, 0xF2006, "spa_wile", NIMI_PONA }, /* alternate wile */
	{{000,000,016,021,001,006,004,012,004,000,000}, 0xF2007, "spa_seme", NIMI_PONA }, /* alternate seme */

	{{003,004,004,004,004,004,004,004,004,004,003}, 0xF1990, "spx_start", NIMI_OPEN, 0,(const char*[]){"space bracketleft","bracketleft","bracketleft space",NULL}}, /* start of cartouche */
	{{070,004,004,004,004,004,004,004,004,004,070}, 0xF1991, "spx_end", NIMI_PINI, 0,(const char*[]){"space bracketright","bracketright","bracketright space",NULL}}, /* end of cartouche */
	{{077,000,000,000,000,000,000,000,000,000,077}, 0xF1992, "spx_line", NIMI_INSA | COMPOSITE | ZWIDTH }, /* cartouche extension */
	{{000,000,004,004,004,004,004,004,004,004,007}, 0xF1993, "spx_pi",0,0,(const char*[]){"p i underscore","p i underscore space", NULL}}, /* start of long pi */
	{{000,000,000,000,000,000,000,000,000,000,077}, 0xF1994, "spx_picont", COMPOSITE | ZWIDTH,0,(const char*[]){"underscore", NULL}}, /* combining long pi extension */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0xF1995, "spx_stackjoiner",ZWIDTH }, /* stacking joiner is ZWJ */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0xF1996, "spx_scalejoiner",ZWIDTH }, /* scaling joiner is ZWJ */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0xF1997, "spx_longstart",ZWIDTH }, /* start of long glyph is ZWJ */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0xF1998, "spx_longend",ZWIDTH }, /* end of long glyph is ZWJ */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0xF1999, "spx_longcont",ZWIDTH }, /* long glyph continuation is ZWJ */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0xF199A, "spx_longstartrev",ZWIDTH }, /* start of rev long glyph is ZWJ */
	{{000,000,000,000,000,000,000,000,000,000,000}, 0xF199B, "spx_longendrev",ZWIDTH }, /* end of rev long glyph is ZWJ */
	{{000,000,000,000,000,004,000,000,000,000,000}, 0xF199C, "spx_dot",0 }, /* scaling joiner is ZWJ */
	{{000,000,000,000,004,000,004,000,000,000,000}, 0xF199D, "spx_colon",0 }, /* scaling joiner is ZWJ */

	/* and finally */
	{{000,000,037,021,021,021,021,021,037,000,000}, -1, ".notdef" },
};

#undef U

#define N_DEFAULT_GLYPHS (sizeof(_glyphs) / sizeof(_glyphs[0]))
static int const _nglyphs = N_DEFAULT_GLYPHS;

static struct glyph* glyphs = NULL;
static int nglyphs = 0;
static struct glyph** glyphs_by_name;

static void dolookups(struct glyph const *);

static int getpix(short const data[YSIZE], int x, int y, unsigned flags) {

	const unsigned int xsize = (flags & DBLXRES) ? XSIZE*2 : XSIZE;

	if (x < 0 || x >= xsize || y < 0 || y >= YSIZE)
		return 0;
	else
		return (data[y] >> (xsize - x - 1)) & 1;
}

static bool plottermode = false;
static bool no_ligatures = false;

static void 
print_class (const char* prefix, unsigned int flags) {
	unsigned int length = 0; //length of all entries

	for (unsigned int i=0; i < nglyphs; i++) {
		if ((glyphs[i].flags & flags)) {
			if (length != 0) length += 1; //add a space after the first glyph name
			length += strlen(glyphs[i].name);
		}
	}

	printf("%s: %u", prefix, length);
	
	for (int i=0; i < nglyphs; i++) {
		if ((glyphs[i].flags & flags)) {
			printf(" %s", glyphs[i].name);
		}
	}
	printf("\n");
}

static char * get_fullname() {
#define FULLNAME_MAX 100
	static char fullname[FULLNAME_MAX];

	sprintf(fullname, "insa pi supa lape%s%s%s%s",
		weight->suffix ? " " : "" , weight->suffix,
		width->suffix ? " " : "" , width->suffix);
	return fullname;
}

static char * fullname_to_fontname(char const *fullname) {
#define FONTNAME_MAX 29 /* Adobe-recommended limit */
	static char fontname[FONTNAME_MAX + 1], *op = fontname;
	char const *p = fullname;
	bool gotfamily = false;

	while (*p != '\0') {
		assert(op - fontname <= FONTNAME_MAX);
		if (*p == ' ') {
			if (!gotfamily) {
				*op++ = '-';
				gotfamily = true;
			}
		} else {
			*op++ = *p;
		}
		++p;
	}
	*op++ = '\0';
	return fontname;
}

static int
compare_glyphs_by_name(const void *va, const void *vb) {
	struct glyph const * const *ap = va, * const *bp = vb;
	struct glyph const *a = *ap, *b = *bp;

	return strcmp(a->name, b->name);
}

static int compare_glyph_to_name(const void *vn, const void *vg) {
	struct glyph const * const *gp = vg;
	struct glyph const *g = *gp;
	char const *name = vn;

	return strcmp(name, g->name);
}

static struct glyph const *get_glyph_by_name(char const *name) {
	struct glyph const * const *gp;

	gp = bsearch(name, glyphs_by_name, nglyphs,
		sizeof(glyphs_by_name[0]), &compare_glyph_to_name);
	assert(gp != NULL);
	return *gp;
}

int main(int argc, char **argv) {
	int i;
	int extraglyphs = 0;
	char *endptr;

	if (argc == 2 && strcmp(argv[1], "--complement") == 0) {
		glyph_complement();
		return 0;
	}

	while (argc > 1) {
		for (i = 0; i < nwidths; i++)
			if (strcmp(argv[1], widths[i].option) == 0) {
				width = &widths[i];
				argv++; argc--;
				goto next;
			}
		for (i = 0; i < nweights; i++)
			if (strcmp(argv[1], weights[i].option) == 0) {
				weight = &weights[i];
				argv++; argc--;
				goto next;
			}
		if (strcmp(argv[1], "--plotter") == 0) {
			plottermode = true;
			argv++; argc--;
		} else if (strcmp(argv[1], "--no-ligatures") == 0) {
			no_ligatures = true;
			argv++; argc--;
		} else if (strcmp(argv[1], "--") == 0) {
			argv++; argc--;
			break;
		} else if (argv[1][0] == '-') {
			fprintf(stderr, "unknown option '%s'\n", argv[1]);
			return 1;
		} else break;
		argv++; argc--;
next:;
	}

	if (argc > 1) {
		short data[YSIZE];
		int i, y;
		unsigned long u;

		for (y = 0; y < YSIZE; y++)
			data[y] = 0;

		y = 0;
		for (i = 1; i < argc; i++) {
			if (y >= YSIZE) {
				fprintf(stderr, "too many arguments\n");
				return 1;
			}
			u = strtoul(argv[i], &endptr, 0);
			if (u > 077 || !argv[i] || *endptr) {
				fprintf(stderr, "invalid argument \"%s\"\n",
						argv[i]);
				return 1;
			}
			data[y++] = u;
		}
		dochar(data, 0);
		return 0;
	}

	// Figure out how many precomposed glyphs we need
	unsigned int compchar_count = 0;
	unsigned int compglyph_count = 0;
	for (i = 0; i < _nglyphs; i++) {
		if (_glyphs[i].flags & NIMI_PONA) {
			compchar_count++;
			compglyph_count += 2;
			//we need one glyph for a cartouched character
			//and another for a pi-d one
		}
	}
	nglyphs = _nglyphs + compglyph_count;
	glyphs = malloc(sizeof(struct glyph) * nglyphs);
	memset(glyphs, 0, sizeof(struct glyph) * nglyphs);
	for (i = 0; i < _nglyphs; i++)
		memcpy(&glyphs[i],&_glyphs[i],sizeof(struct glyph));
	
	unsigned int ci = _nglyphs;
	for (i = 0; i < _nglyphs; i++) {
		if (_glyphs[i].flags & NIMI_PONA) 
		{
			// -- under-and-overlined cartouche composite
			memcpy(&glyphs[ci],&glyphs[i],sizeof(struct glyph));
			glyphs[ci].data[0] |= glyphs[ci].flags & DBLXRES ? 07777 : 077;
			glyphs[ci].data[10] |= glyphs[ci].flags & DBLXRES ? 07777 : 077;
			glyphs[ci].unicode = 0xF3000 + ci;
			char* cart_name = malloc(strlen(glyphs[i].name) + 7);
			snprintf(cart_name,strlen(glyphs[i].name)+7,"%s.cart",glyphs[i].name);
			glyphs[ci].name = cart_name;
			glyphs[ci].flags |= ( NIMI_INSA | COMPOSED );
			glyphs[ci].flags &= ~NIMI_PONA;
			// -- underlined "pi" composite
			unsigned int ci2 = ci + compchar_count; 
			memcpy(&glyphs[ci2],&glyphs[i],sizeof(struct glyph));
			glyphs[ci2].data[10] |= glyphs[ci2].flags & DBLXRES ? 07777 : 077;
			glyphs[ci2].unicode = 0xF3000 + ci2;
			char* pi_name = malloc(strlen(glyphs[i].name) + 5);
			snprintf(pi_name,strlen(glyphs[i].name)+5,"%s.pi",glyphs[i].name);
			glyphs[ci2].name = pi_name;
			glyphs[ci2].flags |= COMPOSED;
			glyphs[ci2].flags &= ~NIMI_PONA;
			ci++;
		}
	}

	// i don't keep track of memory here, but mostly because
	// this is a small tool that's gonna do its job and terminate
	// peacefully at the end anyway.

	glyphs_by_name = malloc(sizeof(struct glyph *) * nglyphs);
	memset(glyphs_by_name, 0, sizeof(struct glyph *) * nglyphs);
		
	for (i = 0; i < nglyphs; i++)
		if (glyphs[i].unicode == -1)
			extraglyphs++;
	printf("SplineFontDB: 3.0\n");
	printf("FontName: %s\n", fullname_to_fontname(get_fullname()));
	printf("FullName: %s\n", get_fullname());
	printf("FamilyName: insa pi supa lape\n");
	printf("LangName: 1033 \"\" \"insa pi supa lape%s%s%s%s\" \"%s\" \"\" \"\" \"\" "
			"\"\" \"\" \"\" \"\" \"\" \"\" \"\" \"\" \"\" \"\" "
			"\"%s\" \"%s%s\"\n\n", weight->suffix[0] ? " " : "" , weight->suffix, 
			width->suffix[0] ? " " : "" , width->suffix,
			"Regular",
			weight->suffix[0] || width->suffix[0] ? "insa pi supa lape" : "",
			weight->suffix[0] ? weight->suffix+1 : "",
			weight->suffix[0] ? width->suffix :
			width->suffix[0] ? width->suffix+1 : "");
	printf("Weight:%s\n", weight->suffix[0] ? weight->suffix : " Medium");
	printf("OS2_WeightWidthSlopeOnly: 1\n");
	printf("Copyright: Dedicated to the public domain\n");
	printf("Version: " FONT_VERSION "\n");
	printf("ItalicAngle: 0\n");
	printf("UnderlinePosition: %g\n", (double)(-YPIX / 2));
	printf("UnderlineWidth: %g\n", (double)(YPIX));
	printf("OS2StrikeYPos: %d\n", (int)(3 * YPIX));
	printf("OS2StrikeYSize: %d\n", (int)(YPIX));
	printf("Ascent: %g\n", (double)(8 * YPIX));
	printf("Descent: %g\n", (double)(2 * YPIX));
	/* Sub/Superscript are three by five pixels */
	printf("OS2SubXSize: %d\n", (int)(YSIZE * YPIX * 3 / (XSIZE - 1)));
	printf("OS2SupXSize: %d\n", (int)(YSIZE * YPIX * 3 / (XSIZE - 1)));
	printf("OS2SubYSize: %d\n", (int)(YSIZE * YPIX * 5 / (YSIZE - 3)));
	printf("OS2SupYSize: %d\n", (int)(YSIZE * YPIX * 5 / (YSIZE - 3)));
	printf("OS2SubXOff: 0\n");
	printf("OS2SupXOff: 0\n");
	printf("OS2SubYOff: %d\n", (int)(2 * YPIX));
	printf("OS2SupYOff: %d\n", (int)(2 * YPIX));
	printf("FSType: 0\n");
	printf("TTFWeight: %d\n", weight->ttfweight);
	printf("TTFWidth: %d\n", width->ttfwidth);
	dopanose();
	printf("OS2FamilyClass: %d\n", 0x080a);
	printf("LayerCount: 2\n");
	printf("Layer: 0 0 \"Back\" 1\n");
	printf("Layer: 1 0 \"Fore\" 0\n");
	printf("Encoding: UnicodeFull\n");
	printf("NameList: Adobe Glyph List\n");
	printf("DisplaySize: -24\n");
	printf("AntiAlias: 1\n");
	printf("FitToEm: 1\n");
	if (plottermode) {
		printf("StrokedFont: 1\n");
		printf("StrokeWidth: 50\n");
	}
	printf("BeginPrivate: 5\n");
	printf(" StdHW 6 [%4g]\n", (double)YPIX);
	printf(" StdVW 6 [%4g]\n",
		(double)(XPIX * (100 + weight->weight) / 100));
	printf(" BlueValues 35 [0 0 %4g %4g %4g %4g %4g %4g]\n",
		(double)(YPIX * 5), (double)(YPIX * 5),
		(double)(YPIX * 6), (double)(YPIX * 6),
		(double)(YPIX * 7), (double)(YPIX * 7));
	printf(" OtherBlues 21 [%4g %4g %4g %4g]\n",
		(double)(YPIX * -2), (double)(YPIX * -2),
		(double)(YPIX * 1), (double)(YPIX * 1));
	printf(" BlueFuzz 1 0\n"); 
	printf("EndPrivate\n");
	/* Force monochrome at 10 and 20 pixels, and greyscale elsewhere. */
	printf("GaspTable: 5 9 2 10 0 19 3 20 0 65535 3\n");
	printf("Lookup: 4 0 1 \"'liga' sitelen pona\" { \"sitelen pona-1\"  } "
			"['liga' ('latn' <'dflt' > ) ]\n");
	printf("Lookup: 1 0 0 \"alternates\" { \"alternates-1\"  } ['cv01' ('latn' <'dflt' > ) ]\n");
	printf("Lookup: 1 0 0 \"pi composites\" { \"pi composites-1\"  } ['ss19' ('DFLT' <'dflt' > 'latn' <'dflt' > ) ]\n");
	printf("Lookup: 6 0 0 \"cartouches\" { \"cartouches-1\"  } ['calt' ('DFLT' <'dflt' > 'latn' <'dflt' > ) ]\n");
	printf("Lookup: 260 0 0 \"'mark' Composite Marks\" { \"'mark' Composite Marks-1\"  } ['mark' ('DFLT' <'dflt' > 'latn' <'dflt' > ) ]\n");
	printf("MarkAttachClasses: 1\n");
	printf("ChainSub2: class \"cartouches-1\" 2 2 0 1\n");
	print_class("  Class",NIMI_PONA);
	print_class("  BClass",NIMI_OPEN | NIMI_INSA);
	printf(" 1 1 0\n");
	printf("  ClsList: 1\n");
	printf("  BClsList: 1\n");
	printf("  FClsList:\n");
	printf(" 1\n");
	printf("  SeqLookup: 0 \"pi composites\"\n");
	printf("  ClassNames: \"etc\" \"nimi_pona\"\n");
	printf("  BClassNames: \"etc\" \"nimi_open_insa\"\n");
	printf("EndFPST\n");
	printf("AnchorClass2: \"composite\" \"'mark' Composite Marks-1\"\n");
	printf("BeginChars: %d %d\n", 0x110000 + extraglyphs, nglyphs);
	extraglyphs = 0;
	for (i = 0; i < nglyphs; i++)
		glyphs_by_name[i] = glyphs + i;
	qsort(glyphs_by_name, nglyphs, sizeof(glyphs_by_name[0]),
			&compare_glyphs_by_name);
	for (i = 0; i < nglyphs; i++) {
		printf("\nStartChar: %s\n", glyphs[i].name);
		printf("Encoding: %d %d %d\n",
				glyphs[i].unicode != -1 ? glyphs[i].unicode :
				0x110000 + extraglyphs++, glyphs[i].unicode, i);
		//sitelen pona specific
		if (glyphs[i].flags & ZWIDTH) { //composite glyphs
			printf("Width: 0\n");
		} else {
			printf("Width: %g\n", (double)(XSIZE * XPIX));
		}
		//
		if (glyphs[i].flags & (MOS6|MOS4))
			printf("Flags: W\n");
		else
			printf("Flags: HW\n");
		if (glyphs[i].flags & NIMI_PONA)
			printf("AnchorPoint: \"composite\" 300 300 basechar 0\n");
		else if (glyphs[i].flags & COMPOSITE)
			printf("AnchorPoint: \"composite\" 300 300 mark 0\n");
		printf("LayerCount: 2\n");
		dolookups(&glyphs[i]);
		if (glyphs[i].alias_of != NULL)
			doalias(glyphs[i].alias_of);
		else if	(glyphs[i].flags & MOS6)
			domosaic(glyphs[i].data[0],
					(glyphs[i].flags & SEP) != 0);
		else if (glyphs[i].flags & MOS4)
			domosaic4(glyphs[i].data[0],
					(glyphs[i].flags & SEP) != 0);
		else {
			if (plottermode)
				dochar_plotter(glyphs[i].data, glyphs[i].flags);
			else
				dochar(glyphs[i].data, glyphs[i].flags);
		}
		printf("EndChar\n");
	}
	printf("EndChars\n");
	printf("EndSplineFont\n");
	return 0;
}

static void dopanose() {
	/*
	 * PANOSE is a complex font categorisation scheme.  I suspect
	 * no-one uses it, but it's a mandatory part of the 'OS/2'
	 * table, so it would be nice to get it right.  This procedure
	 * calculates the PANOSE code for a Bedstead variant based on
	 * its design parameters.
	 *
	 * See <http://panose.com> for the full details.
	 */
	int panose[10];
	int stdvw;
	/* Common digits */
	/* WeightRat == 700/StdVW */
	stdvw = XPIX * (100 + weight->weight) / 100;
	if (stdvw <= 20) /* WeightRat >= 35 */
		panose[2] = 2; /* Very Light */
	else if (stdvw < 39) /* WeightRat > 17.9 */
		panose[2] = 3; /* Light */
	else if (stdvw <= 70) /* WeightRat >= 10 */
		panose[2] = 4; /* Thin */
	else if (stdvw <= 94) /* WeightRat > 7.44 */
		panose[2] = 5; /* Book */
	else if (stdvw < 128) /* WeightRat > 5.5 */
		panose[2] = 6; /* Medium */
	else if (stdvw < 156) /* WeightRat > 4.5 */
		panose[2] = 7; /* Demi */
	else if (stdvw <= 200) /* WeightRat >= 3.5 */
		panose[2] = 8; /* Bold */
	else if (stdvw <= 280) /* WeightRat >= 2.5 */
		panose[2] = 9; /* Heavy */
	else if (stdvw <= 350) /* WeightRat >= 2.0 */
		panose[2] = 10; /* Black */
	else
		panose[2] = 11; /* Extra Black */
	/*
	 * To make life simpler, ignore diagonals when calculating
	 * ConRat.
	 */
	/* ConRat == min(StdVW / 100, 100 / StdVW) */
	if (stdvw > 80 && stdvw < 125)
		panose[4] = 2; /* None */
	else if (stdvw > 65 && stdvw < 154)
		panose[4] = 3; /* Very Low */
	else if (stdvw > 48 && stdvw < 209)
		panose[4] = 4; /* Low */
	else if (stdvw > 30 && stdvw < 334)
		panose[4] = 5; /* Medium Low */
	else if (stdvw > 20 && stdvw < 500)
		panose[4] = 6; /* Medium */
	else if (stdvw > 15 && stdvw < 667)
		panose[4] = 7; /* Medium High */
	else if (stdvw > 8 && stdvw < 1250)
		panose[4] = 8; /* High */
	else
		panose[4] = 9; /* Very High */
	/*
	 * First digit is overall style.  Bedstead is a text font, but
	 * PANOSE says that fonts with horizontal stress and extreme
	 * aspect ratios should be classified as decorative.
	 */
	if (stdvw < 100 || /* ConRat > 1.00 */
			XPIX > 164) { /* ORat < 0.85 */
		panose[0] = 4; /* Latin Decorative */
		panose[1] = 2; /* Derivative */
		/* ORat == 140/XPIX */
		if (XPIX <= 53)
			panose[3] = 2; /* Super Condensed */
		else if (XPIX <= 66)
			panose[3] = 3; /* Very Condensed */
		else if (XPIX <= 110)
			panose[3] = 4; /* Condensed */
		else if (XPIX <= 152)
			panose[3] = 5; /* Normal */
		else if (XPIX <= 155)
			panose[3] = 6; /* Extended */
		else if (XPIX <= 164)
			panose[3] = 7; /* Very Extended */
		else
			panose[3] = 8; /* Super Extended */
		panose[5] = 11; /* Normal Sans Serif */
		panose[6] = 2; /* Standard Solid Fill */
		panose[7] = 2; /* No Lining */
		panose[8] = 3; /* Square */
		panose[9] = 2; /* Extended Collection */
	} else {
		panose[0] = 2; /* Latin Text */
		panose[1] = 11; /* Normal Sans Serif */
		/* J and M are the same width. */
		panose[3] = 9; /* Monospaced */
		if (panose[4] == 2)
			panose[5] = 2; /* No Variation */
		else if (stdvw > 100)
			panose[5] = 5; /* Gradual/Vertical */
		else
			panose[5] = 6; /* Gradual/Hoizontal */
		/* Unusually shaped 'A' means no fit here. */
		panose[6] = 1; /* No Fit */
		/* OutCurv == 0.791 */
		/* FIXME: Only correct for weight == 0 */
		panose[7] = 5; /* Normal/Boxed */
		panose[8] = 3; /* Standard/Pointed */
		panose[9] = 7; /* Ducking/Large */
	}
	printf("Panose: %d %d %d %d %d %d %d %d %d %d\n",
			panose[0], panose[1], panose[2], panose[3], panose[4], 
			panose[5], panose[6], panose[7], panose[8], panose[9]);
}

static void dopalt(struct glyph const *g) {
	int i;
	unsigned char cols = 0;
	int dx = 0, dh = 0;

	while (g->alias_of != NULL)
		g = get_glyph_by_name(g->alias_of);
	if (g->flags & (MOS6|MOS4)) return;
	/*
	 * For proportional layout, we'd like a left side-bearing of
	 * one pixel, and a right side-bearing of zero.  Space
	 * characters get an advance width of three pixels.
	 */
	for (i = 0; i < YSIZE; i++)
		cols |= g->data[i] & ((1 << XSIZE) - 1);
	if (cols == 0)
		dh = 3 - XSIZE;
	else {
		while (!(cols & 1 << (XSIZE - 2))) {
			cols <<= 1;
			dx--;
		}
		while (!(cols & 1)) {
			cols >>= 1;
			dh--;
		}
	}
	if (dx || dh)
		printf("Position2: \"palt\" dx=%d dy=0 dh=%d dv=0\n",
				(int)(dx * XPIX), (int)(dh * XPIX));
}

static void doligature(const char* l_name) {
	printf("Ligature2: \"sitelen pona-1\" %s\n",l_name);

}

static void dolookups(struct glyph const *g) {
	/* sitelen pona specific stuff */
	if ( (g->flags & NIMI_PONA) && (!(g->flags & COMPOSED)) )
		printf("Substitution2: \"pi composites-1\" %s.cart\n", g->name);

	if ((g->unicode >= 0xF1900) && (g->unicode <= 0xFFF00)) {

		if ( (!no_ligatures) && (!(g->flags & COMPOSED)) ) {

			if ( (strlen(g->name) > 3) && (strncmp(g->name,"sp_",3) == 0) ) {

				char ligname[128];
				ligname[0] = 0;
				for (int i=3; i < strlen(g->name); i++) {
					ligname[(i-3)*2] = g->name[i];
					ligname[(i-3)*2+1] = (i < (strlen(g->name)-1)) ? ' ' : 0;
				}
				doligature(ligname);
				strcat(ligname, " space");
				doligature(ligname);
			}
			if (g->substitute != NULL) {
				printf("Substitution2: \"alternates-1\" %s\n",g->substitute);
			}
			if (g->alt_ligatures) {
				int i=0;
				while (g->alt_ligatures[i] != NULL) {
					doligature(g->alt_ligatures[i]);
					i++;
				}
			}

		}
	} else {

		//for some reason, fontforge segfaults when dealt with sitelen pona characters that have a palt

		dopalt(g);
	}
}

typedef struct vec {
	int x, y;
} vec;

typedef struct point {
	struct point *next, *prev;
	struct vec v;
} point;

#define MAXPOINTS (XSIZE * 2 * YSIZE * 20)

static point points[MAXPOINTS];

static int nextpoint;

static void clearpath() {

	nextpoint = 0;
}

static void moveto(unsigned x, unsigned y) {
	struct point *p = &points[nextpoint++];

	p->v.x = x; p->v.y = y;
	p->next = p->prev = NULL;
}

static void lineto(unsigned x, unsigned y) {
	struct point *p = &points[nextpoint++];

	p->v.x = x; p->v.y = y;
	p->next = NULL;
	p->prev = p - 1;
	p->prev->next = p;
}

static void closepath() {
	struct point *p = &points[nextpoint - 1];

	while (p->prev) p--;
	p->prev = points + nextpoint - 1;
	points[nextpoint - 1].next = p;
}

static void killpoint(point *p) {

	p->prev->next = p->next;
	p->next->prev = p->prev;
	p->next = p->prev = NULL;
}

static vec const zero = { 0, 0 };

static int vec_eqp(vec v1, vec v2) {
	return v1.x == v2.x && v1.y == v2.y;
}

static vec vec_sub(vec v1, vec v2) {
	vec ret;
	ret.x = v1.x - v2.x; ret.y = v1.y - v2.y;
	return ret;
}

static int gcd(int a, int b) {
	int t;
	while (b != 0) {
		t = b;
		b = a % b;
		a = t;
	}
	return a;
}

/* Return the shortest representable vector parallel to the argument. */
static vec vec_bearing(vec v) {
	vec ret;
	int d = gcd(abs(v.x), abs(v.y));
	if (d != 0) {
		ret.x = v.x / d;
		ret.y = v.y / d;
	} else {
		ret.x = 0;
		ret.y = 0;
	}
	return ret;
}

/* If p is identical to its successor, remove p. */
static void fix_identical(point *p) {
	if (!p->next) return;
	if (vec_eqp(p->next->v, p->v))
		killpoint(p);
}

/* Are a, b, and c distinct collinear points in that order? */
static int vec_inline3(vec a, vec b, vec c) {
	return
		vec_eqp(vec_bearing(vec_sub(b, a)), vec_bearing(vec_sub(c, b))) &&
		!vec_eqp(vec_bearing(vec_sub(b, a)), zero);
}

/* Are a, b, c, and d distinct collinear points in that order? */
static int vec_inline4(vec a, vec b, vec c, vec d) {
	return vec_inline3(a, b, c) && vec_inline3(b, c, d);
}

/* If p is on the line between its predecessor and successor, remove p. */
static void fix_collinear(point *p) {
	if (!p->next) return;
	if (vec_inline3(p->prev->v, p->v, p->next->v))
		killpoint(p);
}

/* If p is the only point on its path, remove p. */
static void fix_isolated(point *p) {
	if (p->next == p)
		killpoint(p);
}

static int done_anything;

static void fix_edges(point *a0, point *b0) {
	point *a1 = a0->next, *b1 = b0->next;

	assert(a1->prev == a0); assert(b1->prev == b0);
	assert(a0 != a1); assert(a0 != b0);
	assert(a1 != b1); assert(b0 != b1);
	if (vec_eqp(vec_bearing(vec_sub(a0->v, a1->v)),
				vec_bearing(vec_sub(b1->v, b0->v))) &&
			(vec_inline4(a0->v, b1->v, a1->v, b0->v) ||
			 vec_inline4(a0->v, b1->v, b0->v, a1->v) ||
			 vec_inline4(b1->v, a0->v, b0->v, a1->v) ||
			 vec_inline4(b1->v, a0->v, a1->v, b0->v) ||
			 vec_eqp(a0->v, b1->v) || vec_eqp(a1->v, b0->v))) {
		a0->next = b1; b1->prev = a0;
		b0->next = a1; a1->prev = b0;
		fix_isolated(a0);
		fix_identical(a0);
		fix_collinear(b1);
		fix_isolated(b0);
		fix_identical(b0);
		fix_collinear(a1);
		done_anything = 1;
	}
}

static void clean_path() {
	int i, j;

	do {
		done_anything = 0;
		for (i = 0; i < nextpoint; i++)
			for (j = i+1; points[i].next && j < nextpoint; j++)
				if (points[j].next)
					fix_edges(&points[i], &points[j]);
	} while (done_anything);
}

static void emit_contour(point *p0,const int flags) {

	const unsigned int xscale = (flags & DBLXRES) ? (XSCALE*2) : XSCALE;

	point *p = p0, *p1;

	do {
		printf(" %g %g %s 1\n",
				(double)p->v.x / xscale,
				(double)p->v.y / YSCALE - 3*YPIX,
				p == p0 && p->next ? "m" : "l");
		p1 = p->next;
		p->prev = p->next = NULL;
		p = p1;
	} while (p);
}

static void emit_path(const int flags) {
	int i, pass;
	point *p;
	bool started = false;

	/*
	 * On the first pass, emit open contours (if there are any).
	 * On the second pass, emit all remaining contours.
	 */
	for (pass = 0; pass <= 1; pass++) {
		for (i = 0; i < nextpoint; i++) {
			p = &points[i];
			if (p->next && (!p->prev || pass == 1)) {
				if (!started) printf("Fore\nSplineSet\n");
				started = true;
				emit_contour(p,flags);
			}
		}
	}
	if (started) printf("EndSplineSet\n");
}

/*
 * To vary the weight of Bedstead, we just vary the thickness of
 * vertical strokes.  More precisely, we pretend that the weight
 * variation is achieved by moving the rising edges of the output
 * waveform of the SAA5050.  This is implemented by moving all left
 * edges left and right.  The code below is not fully general: it can
 * handle cases where the slack in horizontal lines doesn't run out,
 * and one special case where it does.  This means it should work
 */

static void adjust_weight() {
	int i;
	point *p;

	for (i = 0; i < nextpoint; i++) {
		p = &points[i];
		if (p->next == NULL) continue;
		assert(p->prev != NULL);
		/* Move left-edge points horizontally */
		if (p->prev->v.y <= p->v.y && p->v.y <= p->next->v.y)
			p->v.x -= weight->weight;
		/* Move top inner corner points along NE/SW diagonal */
		if (p->prev->v.y < p->v.y && p->v.y > p->next->v.y &&
				p->prev->v.x > p->v.x && p->v.x > p->next->v.x) {
			p->v.x -= weight->weight/2;
			p->v.y -= weight->weight/2;
		}
		/* Move bottom inner corner points along NW/SE diagonal */
		if (p->prev->v.y > p->v.y && p->v.y < p->next->v.y &&
				p->prev->v.x < p->v.x && p->v.x < p->next->v.x) {
			p->v.x -= weight->weight/2;
			p->v.y += weight->weight/2;
		}
	}
}

static void blackpixel(int x, int y, int bl, int br, int tr, int tl) {
	x *= XPIX_S; y *= YPIX_S;

	if (bl)	moveto(x, y);
	else { moveto(x+XQTR_S, y); lineto(x, y+YQTR_S); }
	if (tl) lineto(x, y+YPIX_S);
	else { lineto(x, y+YPIX_S-YQTR_S); lineto(x+XQTR_S, y+YPIX_S); }
	if (tr) lineto(x+XPIX_S, y+YPIX_S);
	else { lineto(x+XPIX_S-XQTR_S, y+YPIX_S);
		lineto(x+XPIX_S, y+YPIX_S-YQTR_S); }
	if (br) lineto(x+XPIX_S, y);
	else { lineto(x+XPIX_S, y+YQTR_S); lineto(x+XPIX_S-XQTR_S, y); }
	closepath();
}

static void whitepixel(int x, int y, int bl, int br, int tr, int tl) {
	x *= XPIX_S; y *= YPIX_S;

	if (bl) {
		moveto(x, y); lineto(x, y+YPIX_S-YQTR_S);
		if (br) { lineto(x+XPIX_S/2, y+YPIX_S/2-YQTR_S);
			lineto(x+XQTR_S, y); }
		else lineto(x+XPIX_S-XQTR_S, y);
		closepath();
	}
	if (tl) {
		moveto(x, y+YPIX_S); lineto(x+XPIX_S-XQTR_S, y+YPIX_S);
		if (bl) { lineto(x+XPIX_S/2-XQTR_S, y+YPIX_S/2);
			lineto(x, y+YPIX_S-YQTR_S); }
		else lineto(x, y+YQTR_S);
		closepath();
	}
	if (tr) {
		moveto(x+XPIX_S, y+YPIX_S); lineto(x+XPIX_S, y+YQTR_S);
		if (tl) { lineto(x+XPIX_S/2, y+YPIX_S/2+YQTR_S);
			lineto(x+XPIX_S-XQTR_S, y+YPIX_S); }
		else lineto(x+XQTR_S, y+YPIX_S);
		closepath();
	}
	if (br) {
		moveto(x+XPIX_S, y); lineto(x+XQTR_S, y);
		if (tr) { lineto(x+XPIX_S/2+XQTR_S, y+YPIX_S/2);
			lineto(x+XPIX_S, y+YQTR_S); }
		else lineto(x+XPIX_S, y+YPIX_S-YQTR_S);
		closepath();
	}
}

static void dochar(short const data[YSIZE], unsigned flags) {
	int x, y;

#define GETPIX(x,y) (getpix(data, (x), (y), flags))
#define L GETPIX(x-1, y)
#define R GETPIX(x+1, y)
#define U GETPIX(x, y-1)
#define D GETPIX(x, y+1)
#define UL GETPIX(x-1, y-1)
#define UR GETPIX(x+1, y-1)
#define DL GETPIX(x-1, y+1)
#define DR GETPIX(x+1, y+1)

	clearpath();
	const unsigned int xsize = (flags & DBLXRES) ? XSIZE*2 : XSIZE;

	for (x = 0; x < xsize; x++) {
		for (y = 0; y < YSIZE; y++) {
			if (GETPIX(x, y)) {
				bool tl, tr, bl, br;

				/* Assume filled in */
				tl = tr = bl = br = true;
				/* Check for diagonals */
				if ((UL && !U && !L) || (DR && !D && !R))
					tr = bl = false;
				if ((UR && !U && !R) || (DL && !D && !L))
					tl = br = false;
				/* Avoid odd gaps */
				if (L || UL || U) tl = true;
				if (R || UR || U) tr = true;
				if (L || DL || D) bl = true;
				if (R || DR || D) br = true;
				blackpixel(x, YSIZE - y - 1, bl, br, tr, tl);
			} else {
				bool tl, tr, bl, br;

				/* Assume clear */
				tl = tr = bl = br = false;
				/* white pixel -- just diagonals */
				if (L && U && !UL) tl = true;
				if (R && U && !UR) tr = true;
				if (L && D && !DL) bl = true;
				if (R && D && !DR) br = true;
				whitepixel(x, YSIZE - y - 1, bl, br, tr, tl);
			}
		}
	}
	clean_path();
	adjust_weight();
	emit_path(flags);
}

static void reverse_contour(point *a) {
	point *tmp;

	while (a->prev != NULL)
		a = a->prev;
	while (a != NULL) {
		tmp = a->next;
		a->next = a->prev;
		a->prev = tmp;
		a = tmp;
	}
}		

/* Join together two points each at the end of a contour */
static void join_ends(point *a, point *b) {
	point *tmp;

	if (a->prev == NULL && b->next == NULL) {
		tmp = a; a = b; b = tmp;
	}
	if (a->prev == NULL)
		reverse_contour(a);
	if (b->next == NULL)
		reverse_contour(b);
	assert(a->next == NULL);
	assert(a->prev != NULL);
	assert(b->prev == NULL);
	assert(b->next != NULL);
	assert(vec_eqp(a->v, b->v));
	a->next = b;
	b->prev = a;
	fix_identical(a); /* Will delete a */
	fix_collinear(b); /* Might delete b */
}

static bool point_endp(point *p) {
	return p->next == NULL || p->prev == NULL;
}

static bool point_deadp(point *p) {
	return p->next == NULL && p->prev == NULL;
}

static void clean_skeleton() {
	int i, j;

	/* Pass 1: join collinear connected segments */
	for (i = 0; i < nextpoint; i++) {
		if (point_deadp(&points[i]))
			continue;
		for (j = 0; j < nextpoint; j++) {
			if (point_deadp(&points[j]))
				continue;
			if (vec_eqp(points[i].v, points[j].v) &&
					points[i].next == NULL && points[j].prev == NULL &&
					vec_inline3(points[i].prev->v, points[i].v,
						points[j].next->v))
				join_ends(&points[i], &points[j]);
			if (point_deadp(&points[i]))
				break;
		}
	}
	/* Pass 2: join any connected segments */
	for (i = 0; i < nextpoint; i++) {
		if (point_deadp(&points[i]))
			continue;
		for (j = i+1; j < nextpoint; j++) {
			if (point_deadp(&points[j]))
				continue;
			if (vec_eqp(points[i].v, points[j].v) &&
					point_endp(&points[i]) && point_endp(&points[j]))
				join_ends(&points[i], &points[j]);
			if (point_deadp(&points[i]))
				break;
		}
	}
}

static void dochar_plotter(short const data[YSIZE], unsigned flags) {
	int x, y;

#define CONNECT(dx, dy) do { \
	moveto(x * XPIX_S, (YSIZE-y-1) * YPIX_S); \
	lineto((x+dx) * XPIX_S, (YSIZE-y-1+dy) * YPIX_S); \
	} while (0)

	clearpath();
	for (x = 0; x < XSIZE; x++) {
		for (y = 0; y < YSIZE; y++) {
			if (GETPIX(x, y)) {
				if (R) CONNECT(1, 0);
				if (D) CONNECT(0, -1);
				if (DR && !D && !R) CONNECT(1, -1);
				if (DL && !D && !L) CONNECT(-1, -1);
				if (!U && !D && !L && !R &&
						!UL && !UR && !DL && !DR)	{
					/* draw a dot */
					CONNECT(0, 0);
				}
			}
		}
	}
	clean_skeleton();
	emit_path(flags);
}

static void tile(int x0, int y0, int x1, int y1) {
	x0 *= XPIX_S; y0 *= YPIX_S;
	x1 *= XPIX_S; y1 *= YPIX_S;
	moveto(x0, y0); lineto(x0, y1); lineto(x1, y1); lineto(x1, y0);
	closepath();
}

static void domosaic(unsigned code, bool sep) {

	clearpath();
	if (code & 1)  tile(0 + sep, 8 + sep, 3, 11);
	if (code & 2)  tile(3 + sep, 8 + sep, 6, 11);
	if (code & 4)  tile(0 + sep, 4 + sep, 3, 8);
	if (code & 8)  tile(3 + sep, 4 + sep, 6, 8);
	if (code & 16) tile(0 + sep, 1 + sep, 3, 4);
	if (code & 32) tile(3 + sep, 1 + sep, 6, 4);
	clean_path();
	emit_path(0);
}

static void domosaic4(unsigned code, bool sep) {

	clearpath();
	if (code & 1) tile(0 + sep, 6 + sep, 3, 11);
	if (code & 2) tile(3 + sep, 6 + sep, 6, 11);
	if (code & 4) tile(0 + sep, 1 + sep, 3, 6);
	if (code & 8) tile(3 + sep, 1 + sep, 6, 6);
	clean_path();
	emit_path(0);
}

static void doalias(char const *alias_of) {
	struct glyph const *g;

	g = get_glyph_by_name(alias_of);
	printf("Refer: %td %d N 1 0 0 1 0 0 1\n", g - glyphs, g->unicode);
}

static int byunicode(const void *va, const void *vb) {
	struct glyph const *a = *(struct glyph const **)va,
		     *b = *(struct glyph const **)vb;

	/* Cast to unsigned so -1 sorts last. */
	if ((unsigned)a->unicode < (unsigned)b->unicode) return -1;
	if ((unsigned)a->unicode > (unsigned)b->unicode) return +1;
	return strcmp(a->name, b->name);
}

static void glyph_complement() {

	int const nrow = 16, ncol=12;
	int i, unicol = 32/nrow, col = -1, row = 0;
	int const nglyphs = sizeof(glyphs) / sizeof(glyphs[0]);
	int npages = 0;
	bool newcol = false;
	struct glyph const *sorted[nglyphs], *g;

	for (i = 0; i < nglyphs; i++)
		sorted[i] = &glyphs[i];
	qsort(sorted, nglyphs, sizeof(sorted[0]), &byunicode);
	printf("%%!PS-Adobe\n");
	printf("%%%%Creator: bedstead\n");
	printf("%%%%Title: insa pi supa lape Glyph Complement\n");
	printf("%%%%LanguageLevel: 2\n");
	printf("/xfont /insapisupalape findfont 20 scalefont def\n");
	printf("/nfont /insapisupalape findfont 10 scalefont def\n");
	printf("/lfont /insapisupalape findfont 4 scalefont def\n");
	printf("/str 50 string def\n");
	printf("/centre {\n");
	printf(" dup stringwidth pop 2 div neg 0 rmoveto show\n");
	printf("} def\n");
	printf("/label {\n");
	printf(" lfont setfont centre\n");
	printf("} def\n");
	/* unicode glyphname  exemplify -- */
	printf("/exemplify {\n");
	printf(" xfont setfont dup 14 14 moveto glyphshow\n");
	printf(" 0 0 moveto 0 40 lineto 40 40 lineto 40 0 lineto closepath\n");
	printf("  1 setlinewidth gsave stroke grestore clip\n");
	printf(" 0.1 setlinewidth\n");
	printf("  14 10 moveto 14 30 lineto 26 30 lineto 26 10 lineto\n");
	printf("  closepath stroke\n");
	printf(" 20 36 moveto str cvs label\n");
	printf(" 20 2 moveto label\n");
	printf("} def\n");
	printf("/colnum {\n");
	printf(" nfont setfont 20 42 moveto centre\n");
	printf("} def\n");
	printf("/rownums {\n");
	printf(" nfont setfont 0 1 15 {\n");
	printf("  dup -40 mul 16.5 add -10 exch moveto\n");
	printf("  16 1 string cvrs show\n");
	printf(" } for\n");
	printf("} def\n");
	printf("/setdistillerparams where\n");
	printf("{ pop << /SubsetFonts false >> setdistillerparams } if\n");
	printf("%%%%EndProlog\n");
	for (i = 0; i < nglyphs; i++) {
		g = sorted[i];
		if (g->unicode / nrow != unicol ||
				(g->unicode == -1 && row == nrow)) {
			if (++col == ncol) {
				printf("grestore showpage\n");
				col = -1;
			}
			unicol = g->unicode / nrow;
			row = 0;
			newcol = true;
		}
		if (col == -1) {
			++npages;
			printf("%%%%Page: %d %d\n", npages, npages);
			printf("gsave 20 700 translate rownums\n");
			col = 0;
			newcol = true;
		}
		if (newcol && g->unicode != -1) {
			printf("gsave %d 0 translate (%03X) colnum grestore\n",
					col * 40, unicol);
			newcol = false;
		}
		printf("gsave %d %d translate ",
				(col * 40),
				-((g->unicode == -1 ? row++ : g->unicode%nrow) * 40));
		if (g->unicode != -1)
			printf("(U+%04X)", g->unicode);
		else
			printf("()");
		printf("/%s ", g->name);
		printf("exemplify grestore\n");
	}
	printf("showpage\n");
	printf("%%%%EOF\n");
}
