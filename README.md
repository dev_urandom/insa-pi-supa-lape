# insa pi supa lape

This is a font for toki pona's "sitelen pona" writing system, derived from
~bjh21's [Bedstead]([200~https://bjh21.me.uk/bedstead/).

While it's still a work in progress, it already has characters for all of toki
pona's official 120 words, as well as some other commonly used words.

Much like Bedstead, this font is compiled from a C program into a FontForge
source file, which in turn is used to generate the font files needed. Check out
the [releases page](https://gitlab.com/dev_urandom/insa-pi-supa-lape/-/releases)
to find the latest version of this font in a form ready for use.

## Variants

There are two variants of this font: standard and "basic". 

* The standard fonts allow the user to display Latin-based toki pona words
  with their corresponding sitelen pona characters.

* The basic fonts don't convert Latin script into sitelen pona. They will only 
  display sitelen pona characters if they are written using their [UCSUR
  codepoints](https://www.kreativekorp.com/ucsur/charts/sitelen.html).

If you don't know which ones to use, **use the standard fonts**.

Each of the variants has a "light", "regular" and "bold" weight and is available
in OpenType and WOFF formats. The former is best suited for your computer, and
WOFF works best for using the font on the web.

## Usage (standard fonts)

To write regular toki pona words, just write them using the Latin alphabet:

> toki! mi pilin pona mute.

If you need to use unofficial words, put them between brackets every
character with an underscore -- for example,

> ma Kanata li suli.

turns into

> ma [kasi alasa nasin awen telo a] li suli.

If you want to use the word "pi", you can either use it as a regular word or
use a "long pi" by adding an underscore **after** it and every character that's
part of it.

For example, 

> ona li jan pi sona mute.

can be turned into

> ona li jan pi\_ sona\_ mute\_ .

or

> ona li jan pi\_sona\_mute\_.

## Usage (basic fonts)

To write toki pona words, use their corresponding UCSUR codepoints.

> `<U+F196C>`!`<U+F1934><U+F194E><U+F1954><U+F193C>`.

will be rendered as "toki! mi pilin pona mute."

To use unofficial words, start with a `<U+F1990>` cartouche opener, and end with
a `<U+F1991>` cartouche closer. The font itself will draw the cartouche
combining characters in the middle.

To use a "long pi", use the `<U+F1993>` character instead of the regular "pi"
character, and then add a `<U+F1994>` compositing "long pi" character **after**
every character that's part of the "long pi" sequence.

Here are some basic texts using these features -- these will only display
properly if you have a UCSUR-capable font installed and your web browser / text
editor knows how to use it:

> 󱤰󱦐󱤗󱤃󱤿󱤈󱥪󱤀󱦑󱤧󱥣. (ma Kanata li suli.)

> 󱥆󱤧󱤑󱦓󱥡󱦔󱤼󱦔. (ona li jan pi sona mute.)

# License

These fonts are released into the public domain as described by the
[Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/)
license.
